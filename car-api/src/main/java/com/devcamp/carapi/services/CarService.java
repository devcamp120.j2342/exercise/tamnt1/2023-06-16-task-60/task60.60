package com.devcamp.carapi.services;

import java.util.Collections;
import java.util.List;
import java.util.Set;

import org.springframework.stereotype.Service;

import com.devcamp.carapi.models.Car;
import com.devcamp.carapi.models.CarType;
import com.devcamp.carapi.repository.CarRepository;

@Service
public class CarService {
    private final CarRepository carRepository;

    public CarService(CarRepository carRepository) {
        this.carRepository = carRepository;
    }

    public void createCars() {
        Car car1 = new Car("10", "BMW");
        Car car2 = new Car("20", "TOYOTA");
        Car car3 = new Car("30", "MAZDA");

        CarType type1 = new CarType("101", "Z4");
        CarType type2 = new CarType("102", "I8");
        CarType type3 = new CarType("201", "Camry");
        car1.getTypes().add(type1);
        car1.getTypes().add(type2);
        car3.getTypes().add(type3);
        carRepository.save(car1);
        carRepository.save(car2);
        carRepository.save(car3);
    }

    public List<Car> getCars() {
        return carRepository.findAll();
    }

    public Set<CarType> getCarType(String carCode) {
        Car car = carRepository.getCarByCarCode(carCode);

        if (car != null) {
            return car.getTypes();
        } else {

            return Collections.emptySet();
        }
    }
}
